import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';  
import { Observable } from 'rxjs';  

@Injectable({
  providedIn: 'root'
})
export class EmpleadoService {

  private baseUrl = 'http://localhost:8080/api/';  
  
  constructor(private http:HttpClient) { }  
  
  getEmpleadoList(): Observable<any> {  
    return this.http.get(`${this.baseUrl}`+'empleados');  
  }  

  createEmpleado(empleado: object): Observable<object> {  
    return this.http.post(`${this.baseUrl}`+'crear-empleado', empleado);  
  }  

  deleteEmpleado(id: number): Observable<any> {  
    return this.http.delete(`${this.baseUrl}/borrar-empleado/${id}`, { responseType: 'text' });  
  }  
  
  getEmpleado(id: number): Observable<Object> {  
    return this.http.get(`${this.baseUrl}/empleado/${id}`);  
  }  
  
  updateEmpleado(id: number, value: any): Observable<Object> {  
    return this.http.post(`${this.baseUrl}/actualizar-empleado/${id}`, value);  
  }  
}
