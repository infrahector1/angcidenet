import { Component, OnInit } from '@angular/core';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { Empleado } from 'src/app/clases/empleado';  
import { Observable,Subject } from "rxjs";  
  
import {FormControl,FormGroup,Validators} from '@angular/forms';  

@Component({
  selector: 'app-add-empleado',
  templateUrl: './add-empleado.component.html',
  styleUrls: ['./add-empleado.component.css']
})
export class AddEmpleadoComponent implements OnInit {

  constructor(private empleadoservice:EmpleadoService) { }  
  
  empleado : Empleado=new Empleado();  
  submitted = false;  
  
  ngOnInit() {  
    this.submitted=false;  
  }  
  
  empleadosaveform=new FormGroup({  
    primerApellido:new FormControl('' , [Validators.required , Validators.maxLength(20) ] ),
    segundoApellido:new FormControl(),
    primerNombre:new FormControl(),
    otroNombre:new FormControl(),
    paisEmpleo:new FormControl(),
    tipoIdentificacion:new FormControl(),
    nroIdentificacion:new FormControl(),
    correoElectronico:new FormControl(),
    fechaIngreso:new FormControl(),
    area:new FormControl(),
    estado:new FormControl(),
    fechaHoraRegistro:new FormControl(),  
  });  
  
  saveEmpleado(saveEmpleado){  
    this.empleado=new Empleado();     
    this.empleado.primerApellido=this.EmpleadoPrimerApellido.value;  
    this.empleado.segundoApellido=this.EmpleadoSegundoApellido.value;  
    this.empleado.primerNombre=this.EmpleadoPrimerNombre.value; 
    this.empleado.otroNombre=this.EmpleadoOtroNombre.value; 
    this.empleado.paisEmpleo=this.EmpleadoPaisEmpleo.value; 
    this.empleado.tipoIdentificacion=this.EmpleadoTipoIdentificacion.value; 
    this.empleado.nroIdentificacion=this.EmpleadoNroIdentificacion.value; 
    this.empleado.correoElectronico=this.EmpleadoCorreoElectronico.value; 
    this.empleado.fechaIngreso=this.EmpleadoFechaIngreso.value; 
    this.empleado.area=this.EmpleadoArea.value;  
    this.empleado.estado=this.EmpleadoEstado.value;
    this.empleado.fechaHoraRegistro=this.EmpleadoFechaHoraRegistro.value;
    this.submitted = true;  
    this.save();  
  }  
  
    
  
  save() {  
    this.empleadoservice.createEmpleado(this.empleado)  
      .subscribe(data => console.log(data), error => console.log(error));  
    this.empleado = new Empleado();  
  }  
  
  get EmpleadoPrimerApellido(){  
    return this.empleadosaveform.get('primerApellido');  
  }  
  
  get EmpleadoSegundoApellido(){  
    return this.empleadosaveform.get('segundoApellido');  
  }  
  
  get EmpleadoPrimerNombre(){  
    return this.empleadosaveform.get('primerNombre');  
  }
  
  get EmpleadoOtroNombre(){  
    return this.empleadosaveform.get('otroNombre');  
  } 

  get EmpleadoPaisEmpleo(){  
    return this.empleadosaveform.get('paisEmpleo');  
  } 

  get EmpleadoTipoIdentificacion(){  
    return this.empleadosaveform.get('tipoIdentificacion');  
  } 


  get EmpleadoNroIdentificacion(){  
    return this.empleadosaveform.get('nroIdentificacion');  
  } 

  get EmpleadoCorreoElectronico(){  
    return this.empleadosaveform.get('correoElectronico');  
  } 
  get EmpleadoFechaIngreso(){  
    return this.empleadosaveform.get('fechaIngreso');  
  } 
  get EmpleadoArea(){  
    return this.empleadosaveform.get('area');  
  } 
  get EmpleadoEstado(){  
    return this.empleadosaveform.get('estado');  
  } 
  get EmpleadoFechaHoraRegistro(){  
    return this.empleadosaveform.get('fechaHoraRegistro');  
  } 



  addEmpleadoForm(){  
    this.submitted=false;  
    this.empleadosaveform.reset();  
  }  

}
