import { Component, OnInit } from '@angular/core';
import { EmpleadoService } from 'src/app/services/empleado.service';
import { Empleado } from 'src/app/clases/empleado';  
import { Observable,Subject } from "rxjs";  
  
import {FormControl,FormGroup,Validators} from '@angular/forms';  


@Component({
  selector: 'app-empleados-list',
  templateUrl: './empleados-list.component.html',
  styleUrls: ['./empleados-list.component.css']
})
export class EmpleadosListComponent implements OnInit {

  constructor(private empleadoservice:EmpleadoService) { }  
  
  empleadosArray: any[] = [];  
  dtOptions: DataTables.Settings = {};  
  dtTrigger: Subject<any>= new Subject();  

  empleados: Observable<Empleado[]>;  
  empleado : Empleado=new Empleado();  
  deleteMessage=false;  
  empleadolist:any;  
  isupdated = false;      

  ngOnInit(): void {
    this.isupdated=false;  
    this.dtOptions = {  
      pageLength: 6,  
      stateSave:true,  
      lengthMenu:[[6, 16, 20, -1], [6, 16, 20, "All"]],  
      processing: true 

    };
    this.empleadoservice.getEmpleadoList().subscribe(data =>{  
      this.empleados =data;  
      this.dtTrigger.next();  
      })  
  
  }
  deleteEmpleado(id: number) {  
    this.empleadoservice.deleteEmpleado(id)  
      .subscribe(  
        data => {  
          console.log(data);  
          this.deleteMessage=true;  
          this.empleadoservice.getEmpleadoList().subscribe(data =>{  
            this.empleados =data  
            })  
        },  
        error => console.log(error));  
  }  

  updateEmpleado(id: number){  
    this.empleadoservice.getEmpleado(id)  
      .subscribe(  
        data => {  
          this.empleadolist=data             
        },  
        error => console.log(error));  
  }  
  
  empleadoupdateform=new FormGroup({  
    id:new FormControl(),
    primerApellido:new FormControl('' , [Validators.required , Validators.maxLength(20) ] ),
    segundoApellido:new FormControl(),
    primerNombre:new FormControl(),
    otroNombre:new FormControl(),
    paisEmpleo:new FormControl(),
    tipoIdentificacion:new FormControl(),
    nroIdentificacion:new FormControl(),
    correoElectronico:new FormControl(),
    fechaIngreso:new FormControl(),
    area:new FormControl(),
    estado:new FormControl(),
    fechaHoraRegistro:new FormControl(),
  });  
  
  updateEmp(updemp){  
    this.empleado=new Empleado();     
    this.empleado.primerApellido=this.EmpleadoPrimerApellido.value;  
    this.empleado.segundoApellido=this.EmpleadoSegundoApellido.value;  
    this.empleado.primerNombre=this.EmpleadoPrimerNombre.value; 
    this.empleado.otroNombre=this.EmpleadoOtroNombre.value; 
    this.empleado.paisEmpleo=this.EmpleadoPaisEmpleo.value; 
    this.empleado.tipoIdentificacion=this.EmpleadoTipoIdentificacion.value; 
    this.empleado.nroIdentificacion=this.EmpleadoNroIdentificacion.value; 
    this.empleado.correoElectronico=this.EmpleadoCorreoElectronico.value; 
    this.empleado.fechaIngreso=this.EmpleadoFechaIngreso.value; 
    this.empleado.area=this.EmpleadoArea.value;  
    this.empleado.estado=this.EmpleadoEstado.value;
    this.empleado.fechaHoraRegistro=this.EmpleadoFechaHoraRegistro.value;
    this.empleado.id=this.EmpleadoId.value;
    console.log(this.EmpleadoId.value);  
     

   this.empleadoservice.updateEmpleado(this.empleado.id,this.empleado).subscribe(  
    data => {       
      this.isupdated=true;  
      this.empleadoservice.getEmpleadoList().subscribe(data =>{  
        this.empleados =data  
        })  
    },  
    error => console.log(error));  
  }  

  get EmpleadoPrimerApellido(){  
    return this.empleadoupdateform.get('primerApellido');  
  }  
  
  get EmpleadoSegundoApellido(){  
    return this.empleadoupdateform.get('segundoApellido');  
  }  
  
  get EmpleadoPrimerNombre(){  
    return this.empleadoupdateform.get('primerNombre');  
  }
  
  get EmpleadoOtroNombre(){  
    return this.empleadoupdateform.get('otroNombre');  
  } 

  get EmpleadoPaisEmpleo(){  
    return this.empleadoupdateform.get('paisEmpleo');  
  } 

  get EmpleadoTipoIdentificacion(){  
    return this.empleadoupdateform.get('tipoIdentificacion');  
  } 


  get EmpleadoNroIdentificacion(){  
    return this.empleadoupdateform.get('nroIdentificacion');  
  } 

  get EmpleadoCorreoElectronico(){  
    return this.empleadoupdateform.get('correoElectronico');  
  } 
  get EmpleadoFechaIngreso(){  
    return this.empleadoupdateform.get('fechaIngreso');  
  } 
  get EmpleadoArea(){  
    return this.empleadoupdateform.get('area');  
  } 
  get EmpleadoEstado(){  
    return this.empleadoupdateform.get('estado');  
  } 
  get EmpleadoFechaHoraRegistro(){  
    return this.empleadoupdateform.get('fechaHoraRegistro');  
  } 

  get EmpleadoId(){  
    return this.empleadoupdateform.get('id');  
  } 

  changeisUpdate(){  
    this.isupdated=false;  
  }
}
