import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddEmpleadoComponent } from './components/add-empleado/add-empleado.component';
import { EmpleadoDetailsComponent } from './components/empleado-details/empleado-details.component';
import { EmpleadosListComponent } from './components/empleados-list/empleados-list.component';


const routes: Routes = [
  { path: '', redirectTo: 'view-empleado', pathMatch: 'full' },  
  { path: 'view-empleado', component: EmpleadosListComponent },  
  { path: 'add-empleado', component: AddEmpleadoComponent },  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
